package org.example;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        Thread t0 = new Thread(() -> {
            Server server = new Server();
            server.start();
        });
        t0.start();

        //Thread.sleep(3000);

        Thread t1 = new Thread(() -> {
            Client client = new Client();
            client.start();
        });
        t1.start();


    }
}