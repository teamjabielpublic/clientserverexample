package org.example;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {


    public void start() {
        String serverAddress = "localhost";
        int serverPort = 1883;

        try {
            // Create a socket to connect to the server
            Socket socket = new Socket(serverAddress, serverPort);
            System.out.println("Connected to server at " + serverAddress + ":" + serverPort);

            // Get input and output streams from the socket
            InputStream input = socket.getInputStream();
            OutputStream output = socket.getOutputStream();

            // Send data to the server
            String requestData = "Hello, server! This is a message from the client.";
            output.write(requestData.getBytes());

            // Read the response from the server
            byte[] buffer = new byte[1024];
            int bytesRead = input.read(buffer);
            String responseData = new String(buffer, 0, bytesRead);
            System.out.println("Received from server: " + responseData);

            // Close the socket
            socket.close();

            //return socket;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
